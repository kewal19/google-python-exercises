#!/usr/bin/python -tt
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

"""Mimic pyquick exercise -- optional extra exercise.
Google's Python Class

Read in the file specified on the command line.
Do a simple split() on whitespace to obtain all the words in the file.
Rather than read the file line by line, it's easier to read
it into one giant string and split it once.

Build a "mimic" dict that maps each word that appears in the file
to a list of all the words that immediately follow that word in the file.
The list of words can be be in any order and should include
duplicates. So for example the key "and" might have the list
["then", "best", "then", "after", ...] listing
all the words which came after "and" in the text.
We'll say that the empty string is what comes before
the first word in the file.

With the mimic dict, it's fairly easy to emit random
text that mimics the original. Print a word, then look
up what words might come next and pick one at random as
the next work.
Use the empty string as the first word to prime things.
If we ever get stuck with a word that is not in the dict,
go back to the empty string to keep things moving.

Note: the standard python module 'random' includes a
random.choice(list) method which picks a random element
from a non-empty list.

For fun, feed your program to itself as input.
Could work on getting it to put in linebreaks around 70
columns, so the output looks better.

"""

import random
import sys
import pprint
import re

def find(s,word):
  return (i+1 for i,m in enumerate(s) if m==word)

def mimic_dict(filename):
  """Returns mimic dict mapping each word to list of words which follow it."""
  file_word_list = open(filename,'r').read().strip().split()
  word_re = re.compile(r'[A-Za-z]+$')
  file_word_list = filter(word_re.match, file_word_list)
  unique_words = set(file_word_list)
  m_dict = {}
  for each in unique_words:
    m_dict[each] = [ file_word_list[index] for index in (i+1 for i,m in enumerate(file_word_list) if m==each) if index < len(file_word_list) ]
  return m_dict


def print_mimic(mimic_dict, word):
  """Given mimic dict and start word, prints 200 random words."""
  choices = mimic_dict[word]
  for each in xrange(200):
    if len(choices) < 2:
      print "{0} {1}".format(word,choices[0])
    else:
      end_limit = len(choices)-1
      print "{0} {1} {2}".format(word,choices[random.randint(0,end_limit)],each)
  return


# Provided main(), calls mimic_dict() and mimic()
def main():
  if len(sys.argv) != 2:
    print 'usage: ./mimic.py file-to-read'
    sys.exit(1)

  dict = mimic_dict(sys.argv[1])
  print_mimic(dict, 'a')


if __name__ == '__main__':
  main()
